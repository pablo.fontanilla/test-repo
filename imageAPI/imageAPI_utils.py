import base64
import io
import properties
import shutil
from PIL import Image
from io import BytesIO
import logging
import os

logger = logging.getLogger("image_api") 
 #Convierte datos de imagen codificados en base 64 a formato imagen
def base64_to_image(image_data):
    logger.info('Decoding base 64 data')
    image_binary = base64.b64decode(image_data)

    return image_binary

def check_listFolder_parameters(args_dict):
    logger.debug("Checking list folder parameters: %s", str(args_dict))
    for key in args_dict:
        if key != 'path':
            logger.debug("Detected invalid parameter: %s", key)
            type_error = "INVALID_PARAMETER"
            error = 'Invalid parameter: ' + key
            return False, type_error, error
        elif len(args_dict['path']) == 0:
            logger.debug("Detected empty path value")
            type_error = "EMPTY_PARAMETER"
            error = 'Empty parameter: ' + key
            return False, type_error, error

    if not args_dict.has_key('path'):
        logger.debug('Missing parameter path')
        type_error = "MISSING_PARAMETERS"
        error = 'Missing parameters'
        return False, type_error, error

    return True, None, None


# Comprueba los datos extraidos del json del POST: path, filename e imageData
def check_image_parameters(args_dict):
    logger.debug("Checking image parameters: %s", str(args_dict))
    for key in args_dict:
        if key != "path" and key != "filename" and key != "imageData":
            logger.debug("Detected invalid parameter: %s", key)
            type_error = "INVALID_PARAMETER"
            error = 'Invalid parameter: ' + key
            return False, type_error, error

        if len(args_dict[key]) == 0:
            logger.debug("Detected empty %s value", key)
            type_error = "EMPTY_PARAMETER"
            error = 'Empty parameter: ' + key
            return False, type_error, error

    if not args_dict.has_key('path') or not args_dict.has_key('filename') or not args_dict.has_key('imageData'):
        logger.debug('Missing parameter path, filename or imageData')
        type_error = "MISSING_PARAMETER"
        error = 'Missing parameters'
        return False, type_error, error

    try:
        formato = args_dict['filename'].split('.')[1]

    except IndexError:
        formato = "invalid format"

    if not formato in properties.image_formats:
        logger.debug('Invalid image or link format')
        type_error = "INCORRECT_FORMAT"
        error = 'Improper file format: invalid image format'
        return False, type_error, error

    return True, None, None


# Comprueba los datos enviados en el GET SetSymbolicImage: ImagePath, ImagefileName, LinkPath y LinkName
def check_symbolicLink_parameters(args_dict):
    logger.debug("Checking symlink parameters: %s", str(args_dict))
    for key in args_dict:
        if key != 'ImagePath' and key != 'ImagefileName' and key != 'LinkPath' and key != 'LinkName':
            logger.debug("Detected invalid parameter: %s", key)
            type_error = "INVALID_PARAMETER"
            error = 'Invalid parameter: invalid value: ' + key
            return False, type_error, error

        if len(args_dict[key]) == 0:
            logger.debug("Detected empty %s value", key)
            type_error = "EMPTY_PARAMETER"
            error = 'Empty parameter: ' + key
            return False, type_error, error

    if not args_dict.has_key('ImagePath') or not args_dict.has_key('ImagefileName') or not args_dict.has_key(
            'LinkPath') or not args_dict.has_key('LinkName'):
        logger.debug('Missing parameter ImagePath, ImagefileName,  LinkPath or LinkName')
        type_error = "MISSING_PARAMETER"
        error = 'Missing parameters'
        return False, type_error, error

    try:
        formato_image = args_dict['ImagefileName'].split('.')[1]
        formato_image_link = args_dict['LinkName'].split('.')[1]

    except IndexError:
        formato_image = "invalid format"

    if not formato_image in properties.image_formats or not formato_image_link in properties.image_formats:
        logger.debug('Invalid image or link format')
        type_error = "INVALID_PARAMETER"
        error = 'Incorrect parameters: invalid image format'
        return False, type_error, error

    return True, None, None


def check_delete_image_parameters(args_dict):
    logger.debug("Checking image parameters: %s", str(args_dict))
    for key in args_dict:
        if key != 'path' and key != 'filename':
            logger.debug("Detected invalid parameter: %s", key)
            type_error = "INVALID_PARAMETER"
            error = 'Invalid parameter: ' + key
            return False, type_error, error

    if not args_dict.has_key('path') or not args_dict.has_key('filename'):
        logger.debug('Missing parameter path or filename')
        type_error = "MISSING_PARAMETER"
        error = 'Missing parameters'
        return False, type_error, error
    else:
        return True, "", ""


def check_delete_folder_parameters(args_dict):
    logger.debug("Evaluating folder parameters: %s", str(args_dict))
    for key, value in args_dict.iteritems():
        logger.debug("Evaluating key: %s and value %s and value type %s", key, value, type(value))
        if key != 'path' and key != 'recursive':
            type_error = "INVALID_PARAMETER"
            error = 'Invalid parameter: ' + key
            result = False
            return result, type_error, error

    if not args_dict.has_key('path'):
        type_error = "MISSING_PARAMETER"
        error = 'Missing path parameter'
        result = False

    else:
        result = True
        error = ""
        type_error = ""

    return result, type_error, error


# Comprueba si un path existe
def check_if_path_exists(relativePath):
    directory = properties.root_path + relativePath
    # directory = relativePath
    logger.debug("Checking existence of directory %s", directory)
    return os.path.isdir(directory)


# Comprueba si un fichero existe
def check_if_file_exists(relativePath, filename):
    directory = properties.root_path + relativePath
    # directory = relativePath + filename
    logger.debug("Checking existence of file %s", directory + filename)
    return os.path.isfile(directory + filename)


def save_image(path, filename, image):
    image_name = properties.root_path + path + filename
    logger.debug("Saving %s image", image_name)
    with open(image_name, 'wb') as f:
        f.write(image)


def list_folder_files(path):
    try:
        FILENAMES = 2  # La funcion os.walk devuelve una tripleta pero solo queremos los ficheros, que estan en la posicion 2
        # Lista de solo ficheros
        logger.debug("Listing files of %s", path)
        files_list = os.walk(properties.root_path + path).next()[FILENAMES]
    except:
        type_error = "INVALID_PARAMETER"
        error = "System error getting file list"
        return False, type_error, error

    return True, files_list


def create_folder(path):
    try:
        logger.info('Making directory: %s', path)
        os.mkdir(properties.root_path + path)
    except OSError:
        type_error = "INVALID_PARAMETER"
        error = "System error creating folder"
        return False, type_error, error


def create_symbolic_link(path_image, filename_image, path_link, filename_link):
    image_path_name = properties.root_path + path_image + filename_image
    link_path_name = properties.root_path + path_link + filename_link

    try:
        os.symlink(image_path_name, link_path_name)
    except OSError:
        type_error = "INVALID_PARAMETER"
        error = "System error creating symbolicLink"
        return False, type_error, error

    return True, None, None


def path_all_slashes(relative_path):
    if not relative_path.replace('/', '').replace('.', ''):
        return True
    else:
        return False


def delete_folder(relative_path, force_recursion):
    relative_path = unicode_to_string(relative_path)
    if not relative_path or path_all_slashes(relative_path):
        return "FAILURE", "EMPTY_PARAMETER", "Path cannot be / or empty"
    absolute_path = properties.root_path + relative_path
    recursion = force_recursion
    logger.debug("Deleting folder with absolute path: %s. Recursion = %s", absolute_path, recursion)
    try:
        if os.path.islink(absolute_path):
            logger.info('Deleting symlink: %s', absolute_path)
            os.unlink(absolute_path)
            return "SUCCESS", "", ""
        elif not check_if_path_exists(relative_path):
            logger.info("Directory %s does not exist", relative_path)
            return "FAILURE", "VALUE_ERROR", "The specified directory does not exist"
        elif is_directory_empty(absolute_path):
            logger.info('Deleting empty directory: %s', absolute_path)
            os.rmdir(absolute_path)
            return "SUCCESS", "", ""
        elif recursion == True:
            logger.info('Recursively deleting non empty directory: %s', absolute_path)
            shutil.rmtree(absolute_path)
            return "SUCCESS", "", ""
        elif recursion == False and not is_directory_empty(absolute_path):
            logger.info('Requested deletion of non empty directory with no recursion option')
            return "FAILURE", "NOT EMPTY FOLDER", "The selected folder is not empty and recursive option has not been selected"
        else:
            return "FAILURE", "OS_ERROR", "There was an unspecified error deleting the folder"
    except Exception:
        logger.exception()
        return "FAILURE", "OS_ERROR", "There was a problem deleting the folder"


# newvariable160 = relative_path
def delete_file(newvariable160, file):
    newvariable160 = unicode_to_string(newvariable160)
    if not newvariable160 or path_all_slashes(newvariable160) or not file:
        return "FAILURE", "EMPTY_PARAMETER", "Path or file cannot be empty"

    path = properties.root_path + newvariable160
    filename = file
    file_path = path + filename
    logger.debug('Working with absolute file: %s', file_path)

    try:
        if os.path.islink(file_path):
            logger.info('Deleting symlink: %s', file_path)
            os.unlink(file_path)
            return "SUCCESS", "", ""
        elif check_if_file_exists(newvariable160, filename):
            logger.info('Deleting regular file %s', file_path)
            os.remove(file_path)
            return "SUCCESS", "", ""
        else:
            logger.info('Could not find file %s', file_path)
            return "FAILURE", "VALUE_ERROR", "Couldn't find file or symlink"

    except Exception as e:
        return "FAILURE", "OS ERROR", "There was a problem deleting the file: " + str(e.message)


def is_directory_empty(directory):
    # if listdir has results, it's not empty
    logger.debug("Testing for empty directory %s", directory)
    try:
        if os.path.exists(directory) and os.path.isdir(directory):
            if len(os.listdir(directory)) != 0:
                logger.debug("Directory not empty")
                return False
            else:
                logger.debug("Directory empty")
                return True
        else:
            return False
    except Exception as e:
        logger.debug("Problem testing for directory emptyness")
        logger.exception(e)
        raise e


def unicode_to_string(string):
    try:
        return string.encode('utf-8')
    except:
        return string
