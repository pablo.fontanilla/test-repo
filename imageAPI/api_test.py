import sys
import pytest
import pytest_check as check
import imageAPI_utils
import imageAPI
import os
import shutil
import auxiliary_test_functions as test_functions
import properties
import sample_image_data


@pytest.fixture
def client():

    imageAPI.app.config['TESTING'] = True
    with imageAPI.app.test_client() as client:
        yield client
    
def test_list_hello_world(client):
    response = client.get('/ImageAPI/helloworld')

    response_data = response.get_json()

    check.is_true(response_data['Hello'] == "Hello, IMAGE API is alive")

def test_list_folder_images(client):
    
    EMPTY_DIR_PATH='/opt/images/empty_dir'
    if not os.path.exists(EMPTY_DIR_PATH):
        os.mkdir(EMPTY_DIR_PATH)    
    check.is_true(test_functions.missing_path_parameter(client))
    check.is_true(test_functions.empty_path(client))
    check.is_true(test_functions.invalid_parameters(client))
    check.is_true(test_functions.extra_parameters(client))
    check.is_true(test_functions.non_existing_directory(client))
    check.is_true(test_functions.no_directory_permissions(client))
    check.is_true(test_functions.empty_directory(client))
    check.is_true(test_functions.no_directory_permissions(client)    )

    shutil.rmtree(EMPTY_DIR_PATH) 
 
def test_upload_image(client):

    TEST_FOLDER_PATH='/tests_upload_image/'
    TEST_FOLDER_FULL_PATH='/opt/images' + TEST_FOLDER_PATH
    if not os.path.exists(TEST_FOLDER_FULL_PATH):
        os.mkdir(TEST_FOLDER_FULL_PATH)    
    API_FUNCTION='UploadImage'
    check.is_true(test_functions.upload_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.duplicated_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_malformed_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_incorrect_format_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_to_wrong_path(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_too_many_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'imageData'))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'filename'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'imageData'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'filename'))
    check.is_true(test_functions.upload_malformed_json(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_missing_json(client,TEST_FOLDER_PATH,API_FUNCTION))
    shutil.rmtree(TEST_FOLDER_FULL_PATH) 
    
def test_update_image(client):
    TEST_FOLDER_PATH='/tests_update_image/'
    TEST_FOLDER_FULL_PATH='/opt/images' + TEST_FOLDER_PATH
    if not os.path.exists(TEST_FOLDER_FULL_PATH):
        os.mkdir(TEST_FOLDER_FULL_PATH)    
    API_FUNCTION='UpdateImage'
    check.is_true(test_functions.update_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_malformed_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_incorrect_format_image(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_to_wrong_path(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_too_many_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'imageData'))
    check.is_true(test_functions.upload_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'filename'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'imageData'))
    check.is_true(test_functions.upload_empty_parameters(client,TEST_FOLDER_PATH,API_FUNCTION,'filename'))
    check.is_true(test_functions.upload_malformed_json(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.upload_missing_json(client,TEST_FOLDER_PATH,API_FUNCTION))
    shutil.rmtree(TEST_FOLDER_FULL_PATH) 

def test_delete_image(client):
    TEST_FOLDER_PATH='/tests_delete_image/'
    TEST_FOLDER_FULL_PATH='/opt/images' + TEST_FOLDER_PATH
    if not os.path.exists(TEST_FOLDER_FULL_PATH):
        os.mkdir(TEST_FOLDER_FULL_PATH)    
    API_FUNCTION='DeleteImage'
    check.is_true(test_functions.delete_item_OK(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_random_item(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_item_empty_parameter(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.delete_item_empty_parameter(client,TEST_FOLDER_PATH,API_FUNCTION,'filename'))
    check.is_true(test_functions.delete_item_too_many_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_item_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    shutil.rmtree(TEST_FOLDER_FULL_PATH) 

def test_delete_folder(client):
    TEST_FOLDER_PATH='/tests_delete_folder/'
    TEST_FOLDER_FULL_PATH='/opt/images' + TEST_FOLDER_PATH
    if not os.path.exists(TEST_FOLDER_FULL_PATH):
        os.mkdir(TEST_FOLDER_FULL_PATH)
    API_FUNCTION='DeleteFolder'
    check.is_true(test_functions.delete_item_OK(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_random_item(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_item_empty_parameter(client,TEST_FOLDER_PATH,API_FUNCTION,'path'))
    check.is_true(test_functions.delete_item_too_many_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_item_missing_parameters(client,TEST_FOLDER_PATH,API_FUNCTION))
    check.is_true(test_functions.delete_folder_root_path(client))
    check.is_true(test_functions.delete_not_empty_folder_no_recursion(client,TEST_FOLDER_PATH))
    check.is_true(test_functions.delete_not_empty_folder_recursion(client,TEST_FOLDER_PATH,'TRUE'))
    check.is_true(test_functions.delete_not_empty_folder_recursion(client,TEST_FOLDER_PATH,'FALSE'))
    check.is_true(test_functions.delete_not_empty_folder_recursion(client,TEST_FOLDER_PATH,'Random_string'))
    shutil.rmtree(TEST_FOLDER_FULL_PATH) 

def test_set_symbolic_image(client):
    
    LINK_FOLDER_PATH='/links_dir/'
    TEST_FOLDER_PATH='/tests_update_image/'
    TEST_FOLDER_FULL_PATH= properties.root_path + TEST_FOLDER_PATH
    LINK_FOLDER_FULL_PATH= properties.root_path + LINK_FOLDER_PATH

    if not os.path.exists(TEST_FOLDER_FULL_PATH):
        os.mkdir(TEST_FOLDER_FULL_PATH)   
    if not os.path.exists(LINK_FOLDER_FULL_PATH):
        os.mkdir(LINK_FOLDER_FULL_PATH)   
    
    check.is_true(test_functions.create_symbolic_link_empty_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagePath'))
    check.is_true(test_functions.create_symbolic_link_empty_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagefileName'))
    check.is_true(test_functions.create_symbolic_link_empty_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkPath'))
    check.is_true(test_functions.create_symbolic_link_empty_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkName'))

    check.is_true(test_functions.create_symbolic_link_missing_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagePath'))
    check.is_true(test_functions.create_symbolic_link_missing_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagefileName'))
    check.is_true(test_functions.create_symbolic_link_missing_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkPath'))
    check.is_true(test_functions.create_symbolic_link_missing_parameter(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkName'))

    check.is_true(test_functions.create_symbolic_link_too_many_parameters(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH))

    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagePath'))
    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagefileName'))
    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkPath'))
    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'LinkName'))
    check.is_true(test_functions.create_existing_symbolic_link(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH))

    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagefileName',create_items=False))
    check.is_true(test_functions.create_symbolic_link_random_parameter_value(client,TEST_FOLDER_PATH,LINK_FOLDER_PATH,'ImagefileName',random_path=True))

    shutil.rmtree(TEST_FOLDER_FULL_PATH) 
    shutil.rmtree(LINK_FOLDER_FULL_PATH) 
