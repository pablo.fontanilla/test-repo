import sys
import pytest
import imageAPI_utils
import imageAPI
import os
import shutil
import sample_image_data
import random
import string
import properties


def get_random_filename(n):
    return ''.join(random.choice(string.ascii_lowercase) for i in range(n))
    # This comment is good for real


def get_image_json(file_format, path):
    image_data = sample_image_data.IMAGE_DATA
    image_name = get_random_filename(5) + '.' + file_format
    image_path = path
    image_json = {'path': image_path, 'filename': image_name, 'imageData': image_data}
    return image_json


def duplicated_image(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']
    image_full_path = properties.root_path + image_json['path']

    # Delete image if it exists so we don't get duplicated error
    if os.path.exists(image_full_path + image_name):
        os.remove(image_full_path + image_name)

    # Create the file
    response = client.post('/ImageAPI/' + URL, json=image_json)
    # Repost for duplicate response
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()

    return response_data['response']['code'] == "DUPLICATED_IMAGE"


def upload_image(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']
    image_full_path = properties.root_path + image_json['path']

    # Delete image if it exists so we don't get duplicated error
    if os.path.exists(image_full_path + image_name):
        os.remove(image_full_path + image_name)

    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    if URL == 'UpdateImage':
        # Upload correct file
        return response_data['response']['status'] == "FAILURE" and response_data['response'][
            'code'] == "INVALID_PARAMETER"
    else:
        return response_data['response']['status'] == "SUCCESS"


def update_image(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']
    image_full_path = properties.root_path + image_json['path']
    # Delete image if it exists so we don't get duplicated error
    if os.path.exists(image_full_path + image_name):
        os.remove(image_full_path + image_name)
    # Create the file
    response = client.post('/ImageAPI/UploadImage', json=image_json)
    # Repost for duplicate response
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()

    return response_data['response']['status'] == "SUCCESS"


def upload_to_wrong_path(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']

    image_full_path = properties.root_path + image_json['path']

    if URL == 'UpdateImage':
        # Upload correct file
        client.post('/ImageAPI/UploadImage', json=image_json)
    # Break path
    image_json['path'] = '/made_up_path'
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INVALID_PARAMETER"


def upload_too_many_parameters(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']

    if URL == 'UpdateImage':
        # Upload correct file
        client.post('/ImageAPI/UploadImage', json=image_json)
    # Add extra param
    image_json['extra_param'] = 'nothing'

    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INVALID_PARAMETER"


def upload_empty_parameters(client, test_path, URL, parameter):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']

    if URL == 'UpdateImage':
        # Upload correct file
        client.post('/ImageAPI/UploadImage', json=image_json)

    image_json[parameter] = ''
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "EMPTY_PARAMETER"


def upload_missing_parameters(client, test_path, URL, parameter):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']

    if URL == 'UpdateImage':
        # Upload correct file
        client.post('/ImageAPI/UploadImage', json=image_json)

    image_json.pop(parameter)
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "MISSING_PARAMETER"


def upload_malformed_json(client, test_path, URL):
    image_json = '123123'
    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INVALID_PARAMETER"


def upload_missing_json(client, test_path, URL):
    response = client.post('/ImageAPI/' + URL)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "MISSING_PARAMETER"


def upload_incorrect_format_image(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)
    image_data = image_json['imageData']
    image_name = image_json['filename']
    image_full_path = properties.root_path + image_json['path']

    if URL == 'UpdateImage':
        # Upload correct file
        client.post('/ImageAPI/UploadImage', json=image_json)
    # Break format
    image_json['filename'] = image_json['filename'][1:-4] + '.foobar'

    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INCORRECT_FORMAT"


def upload_malformed_image(client, test_path, URL):
    image_json = get_image_json('jpg', test_path)

    image_data = image_json['imageData']
    image_name = image_json['filename']
    image_full_path = properties.root_path + image_json['path']

    # Delete image if it exists so we don't get duplicated error
    if os.path.exists(image_full_path + image_name):
        os.remove(image_full_path + image_name)

    if URL == 'UpdateImage':
        # Create the object to be updated, then modify imageData
        client.post('/ImageAPI/UploadImage', json=image_json)
        image_json['imageData'] = 'foobar'
    else:
        image_json['imageData'] = 'foobar'

    response = client.post('/ImageAPI/' + URL, json=image_json)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response'][
        'message'] == "Error in imageData, invalid base64 format"


def invalid_parameters(client):
    response = client.get('/ImageAPI/ListFolderImages?test_parameter=/contributors')
    response_data = response.get_json()
    return response_data['response']['code'] == "INVALID_PARAMETER" and response_data['response'][
        'message'] == "Invalid parameter: test_parameter"


def missing_path_parameter(client):
    response = client.get('/ImageAPI/ListFolderImages')
    response_data = response.get_json()
    return response_data['response']['code'] == "MISSING_PARAMETERS" and response_data['response'][
        'message'] == "Missing parameters"


def empty_path(client):
    response = client.get('/ImageAPI/ListFolderImages?path=')
    response_data = response.get_json()
    return response_data['response']['code'] == "EMPTY_PARAMETER" and response_data['response'][
        'message'] == "Empty parameter: path"


def extra_parameters(client):
    response = client.get('/ImageAPI/ListFolderImages?path=/contributors&test_path=/fallsdfsdfback')
    response_data = response.get_json()
    return response_data['response']['code'] == "INVALID_PARAMETER" and response_data['response'][
        'message'] == "Invalid parameter: test_path"


def non_existing_directory(client):
    response = client.get('/ImageAPI/ListFolderImages?path=/non_existing_directory')
    response_data = response.get_json()
    return response_data['response']['code'] == "INVALID_PARAMETER" and response_data['response'][
        'message'] == "Incorrect path value"


def no_directory_permissions(client):
    # Untestable in test environment due to running as root user. Tested in preproduction
    response = client.get('/ImageAPI/ListFolderImages?path=/test')
    response_data = response.get_json()
    # return response_data['response']['code'] == "INVALID_PARAMETER" and response_data['response']['message'] == "System error getting file list"
    return True


def empty_directory(client):
    response = client.get('/ImageAPI/ListFolderImages?path=/empty_dir')
    response_data = response.get_json()
    return response_data['response']['ImageList'][0] == True and response_data['response']['ImageList'][1] == []


def delete_item_OK(client, test_path, URL):
    item_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    # Create item so we can delete it
    open(full_item_path, "w") if 'DeleteImage' == URL else os.mkdir(full_item_path)

    if 'DeleteImage' == URL:
        response = client.get('/ImageAPI/DeleteImage?path=' + test_path + '&filename=' + item_name)
    else:
        response = client.get('/ImageAPI/DeleteFolder?path=' + test_path + item_name)

    response_data = response.get_json()
    return response_data['response']['status'] == "SUCCESS"


def delete_random_item(client, test_path, URL):
    item_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name

    if 'DeleteImage' == URL:
        response = client.get('/ImageAPI/DeleteImage?path=' + test_path + '&filename=' + item_name)
    else:
        response = client.get('/ImageAPI/DeleteFolder?path=' + test_path + item_name)

    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "VALUE_ERROR"


def delete_item_empty_parameter(client, test_path, function, parameter):
    # Create item so we can delete it
    item_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w") if 'DeleteImage' == function else os.mkdir(full_item_path)

    if 'path' == parameter:
        path = ''
        filename = item_name
    elif 'filename' == parameter:
        path = test_path
        filename = ''

    if 'DeleteImage' == function:
        url = '/ImageAPI/' + function + '?path=' + path + '&filename=' + filename
        response = client.get(url)
    else:
        url = '/ImageAPI/' + function + '?path=' + path
        response = client.get(url)

    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "EMPTY_PARAMETER"


def delete_item_too_many_parameters(client, test_path, function):
    # Create item so we can delete it
    item_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w") if 'DeleteImage' == function else os.mkdir(full_item_path)

    if 'DeleteImage' == function:
        url = '/ImageAPI/' + function + '?path=' + test_path + '&filename=' + item_name + '&randomParam=' + 'randomValue'
        response = client.get(url)
    else:
        url = '/ImageAPI/' + function + '?path=' + test_path + item_name + '&randomParam=' + 'randomValue'
        response = client.get(url)

    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INVALID_PARAMETER"


def delete_item_missing_parameters(client, test_path, function):
    # Create item so we can delete it
    item_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w") if 'DeleteImage' == function else os.mkdir(full_item_path)

    if 'DeleteImage' == function:
        url = '/ImageAPI/' + function + '?path=' + test_path
        url2 = '/ImageAPI/' + function + '?filename=' + item_name
        response = client.get(url)
        response2 = client.get(url2)
        response_data = response.get_json()
        response_data2 = response2.get_json()
        return response_data['response']['code'] == "MISSING_PARAMETER" and response_data2['response'][
            'code'] == "MISSING_PARAMETER"
    else:
        url = '/ImageAPI/' + function
        response = client.get(url)
        response_data = response.get_json()
        return response_data['response']['status'] == "FAILURE" and response_data['response'][
            'code'] == "MISSING_PARAMETER"


def delete_folder_root_path(client):
    paths_list = ['/', '//', '/.', '/../', './']
    response_list = []
    for path in paths_list:
        response = client.get('/ImageAPI/DeleteFolder?path=' + path)
        response_data = response.get_json()
        response_list.append(response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "EMPTY_PARAMETER")

    if False in response_list:
        return False
    else:
        return True

def delete_not_empty_folder_no_recursion(client, test_path):
    folder_name = get_random_filename(5)
    full_folder_path = properties.root_path + test_path + folder_name
    # Create folder and items so we can delete them
    os.mkdir(full_folder_path)

    open(full_folder_path + '/' + get_random_filename(5), "w")
    open(full_folder_path + '/' + get_random_filename(5), "w")

    url = '/ImageAPI/DeleteFolder?path=' + test_path + folder_name
    response = client.get(url)

    response_data = response.get_json()
    return response_data['response']['code'] == "NOT EMPTY FOLDER"


def delete_not_empty_folder_recursion(client, test_path, recursion):
    folder_name = get_random_filename(5)
    full_folder_path = properties.root_path + test_path + folder_name
    # Create folder and items so we can delete them
    os.mkdir(full_folder_path)

    open(full_folder_path + '/' + get_random_filename(5), "w")
    open(full_folder_path + '/' + get_random_filename(5), "w")

    url = '/ImageAPI/DeleteFolder?path=' + test_path + folder_name + '&recursive=' + recursion

    response = client.get(url)

    response_data = response.get_json()
    if 'TRUE' == recursion:
        return response_data['response']['status'] == "SUCCESS"
    else:
        return response_data['response']['code'] == "NOT EMPTY FOLDER"


def create_symbolic_link_empty_parameter(client, test_path, relative_link_path, empty_parameter):
    # Create item so we can link it
    item_name = get_random_filename(5)
    link_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w")

    url_base = '/ImageAPI/' + 'SetSymbolicImage' + '?'

    url_dict = {
        'ImagePath': url_base + 'ImagePath=' + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'ImagefileName': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'LinkPath': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + '&LinkName=' + link_name,
        'LinkName': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName='
    }
    response = client.get(url_dict[empty_parameter])
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "EMPTY_PARAMETER"


def create_symbolic_link_missing_parameter(client, test_path, relative_link_path, missing_parameter):
    # Create item so we can link it
    item_name = get_random_filename(5)
    link_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w")

    url_base = '/ImageAPI/' + 'SetSymbolicImage' + '?'

    url_dict = {
        'ImagePath': url_base + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'ImagefileName': url_base + 'ImagePath=' + test_path + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'LinkPath': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkName=' + link_name,
        'LinkName': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path
    }
    response = client.get(url_dict[missing_parameter])
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "MISSING_PARAMETER"


def create_symbolic_link_random_parameter_value(client, test_path, relative_link_path, random_parameter,
                                                create_items=True, random_path=False):
    # Create item so we can link it
    item_name = get_random_filename(5) + '.jpg'
    link_name = get_random_filename(5) + '.jpg'
    full_item_path = properties.root_path + test_path + item_name
    if create_items:
        open(full_item_path, "w")

    if random_path:
        test_path = '/' + get_random_filename(5) + '/'
    url_base = '/ImageAPI/' + 'SetSymbolicImage' + '?'

    url_dict = {
        'ImagePath': url_base + 'ImagePath=' + get_random_filename(
            5) + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'ImagefileName': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + get_random_filename(
            5) + '.jpg' + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name,
        'LinkPath': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + get_random_filename(
            5) + '/&LinkName=' + link_name,
        'LinkName': url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + get_random_filename(
            5) + '.jpg'
    }
    response = client.get(url_dict[random_parameter])
    response_data = response.get_json()
    if random_parameter is not 'LinkName' and random_parameter is not 'LinkPath':
        return response_data['response']['code'] == "INVALID_PARAMETER"
    else:
        return response_data['response']['status'] == "SUCCESS"


def create_symbolic_link_too_many_parameters(client, test_path, relative_link_path):
    # Create item so we can link it
    item_name = get_random_filename(5)
    link_name = get_random_filename(5)
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w")

    url = '/ImageAPI/' + 'SetSymbolicImage' + '?ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name + '&random_parameter=123123'

    response = client.get(url)
    response_data = response.get_json()
    return response_data['response']['status'] == "FAILURE" and response_data['response']['code'] == "INVALID_PARAMETER"


def create_existing_symbolic_link(client, test_path, relative_link_path):
    # Create item so we can link it
    item_name = get_random_filename(5) + '.jpg'
    link_name = get_random_filename(5) + '.jpg'
    full_item_path = properties.root_path + test_path + item_name
    open(full_item_path, "w")

    url_base = '/ImageAPI/' + 'SetSymbolicImage' + '?'
    url_full = url_base + 'ImagePath=' + test_path + '&ImagefileName=' + item_name + '&LinkPath=' + relative_link_path + '&LinkName=' + link_name

    response_1 = client.get(url_full)
    response_2 = client.get(url_full)

    response_data = response_2.get_json()
    return response_data['response']['message'] == "Invalid Link: link already exists" and response_data['response'][
        'code'] == "INVALID_PARAMETER"
