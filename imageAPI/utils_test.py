import sys
import unittest
import os
import random
import datetime
import string
import shutil
import imageAPI_utils
import properties

class UtilsTests(unittest.TestCase):

    def setUp(self):
        #Create a temporary folder to run all tests
        self.home_test_folder_name = '/tests/' +''.join(random.choice(string.ascii_lowercase) for i in range(5))
        os.makedirs(properties.root_path + self.home_test_folder_name)
        
    def tearDown(self):
        shutil.rmtree(properties.root_path + self.home_test_folder_name)
        
    def test_check_path_file_existence(self):
        #Create name and relative path variables
        test_folder_name = "/test_1234"
        test_file_name = "/test_1234_file.txt"
        test_path= self.home_test_folder_name
       
        #Negative tests for existance
        self.assertFalse(imageAPI_utils.check_if_path_exists(test_path + test_folder_name)) # Folder doesn't exist
        self.assertFalse(imageAPI_utils.check_if_path_exists(test_path + test_file_name)) # File is not a folder

        #Create folder and file for tests
        open(properties.root_path + test_path + test_file_name,"w+")
        os.mkdir(properties.root_path + test_path + test_folder_name)

        #Positive tests for existance
        self.assertTrue(imageAPI_utils.check_if_path_exists(test_path + test_folder_name))
        self.assertTrue(imageAPI_utils.check_if_file_exists(test_path,test_file_name))

    def test_list_folder_files(self):
        file_names = list()
        
        boolean_result,empty_file_list = imageAPI_utils.list_folder_files(self.home_test_folder_name)


        
        for file in range(5):
            file = '/' +''.join(random.choice(string.ascii_lowercase) for i in range(10))
            open(properties.root_path + self.home_test_folder_name + file,"w+")
            file_names.append(file)

        expected_file_list = file_names
        boolean_result, returned_file_list = imageAPI_utils.list_folder_files(self.home_test_folder_name)

        returned_file_list = list(map(lambda x: x.strip('/'),returned_file_list))
        expected_file_list = list(map(lambda x: x.strip('/'),expected_file_list))

        self.assertEqual(empty_file_list,[])
        self.assertEqual(expected_file_list,returned_file_list)
        


        
        
    