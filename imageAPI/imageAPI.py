import io
import os
import properties as prop
import time
from PIL import Image
import imghdr
from flask import (Flask, abort, jsonify, make_response, request, send_from_directory)
import imageAPI_utils
import logging
import sys
#SERVER_URL = "/imageAPI/"
logger = logging.getLogger("image_api")
def setup_logger():
        logger.setLevel(logging.DEBUG)
        log_path = prop.log_path
        log_file = log_path + prop.log_file
        h1 = logging.FileHandler(filename=log_file)
        formatter = logging.Formatter('%(asctime)s %(message)s')
        h1.setFormatter(formatter)
        h1.setLevel(prop.LOG_LEVEL_FILE)
        h2 = logging.StreamHandler(sys.stderr)
        h2.setLevel(prop.LOG_LEVEL_CONSOLE)
        logger.addHandler(h1)
        logger.addHandler(h2)

app = Flask(__name__)


setup_logger()

@app.errorhandler(400)
def bad_request(e):
    return make_response(str(e), 400)


@app.errorhandler(404)
def resource_not_found(e):
    return make_response(str(e), 404)


@app.errorhandler(500)
def server_error(e):
    return make_response(str(e), 500)


@app.route(prop.SERVER_URL + "helloworld", methods=['GET'])
def hello_world():
    #logger.debug("Starting Hello, World!...")
    return jsonify({"Hello": "Hello, IMAGE API is alive"}), 200


@app.route(prop.SERVER_URL + "ListFolderImages", methods=['GET'])
def list_folder_images():
    function_type="function_type"
    logger.info('Processing list folder images request')
    args_dict = request.args.to_dict()
    logger.debug('Received arguments: %s', str(args_dict))

    check_result = imageAPI_utils.check_listFolder_parameters(args_dict)
    
    if check_result[0] == False:
        response = build_response("ListFolderImages", "FAILURE", check_result[1], check_result[2] )
        return jsonify(response)


    path_exists = imageAPI_utils.check_if_path_exists(args_dict['path'])
    #path_exist=comprobacion de path (path)
    if not path_exists:
        logger.debug("Error, Incorrect path value")
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Incorrect path value" )
        return jsonify(response)

    list_folder_response = imageAPI_utils.list_folder_files(args_dict['path'])
    if list_folder_response[0] == False:
        response = build_response("ListFolderImages", "FAILURE", list_folder_response[1], list_folder_response[2] )
        return jsonify(response)

    response = build_response("ListFolderImages", "SUCCESS")
    response['response']['ImageList']=list_folder_response

    return jsonify(response)



@app.route(prop.SERVER_URL + "SetSymbolicImage", methods=['GET'])
def set_symbolic_image():
    logger.info('Processing set symbolic images request')
    args_dict = request.args.to_dict()
    function_type = "SetSymbolicImage"
    logger.debug('Received arguments: %s', str(args_dict))

    check_result = imageAPI_utils.check_symbolicLink_parameters(args_dict)
    
    if check_result[0] == False:
        response = build_response("SetSymbolicImage", "FAILURE", check_result[1], check_result[2] )
        return jsonify(response)

    path_exists = imageAPI_utils.check_if_path_exists(args_dict['ImagePath'])
    #path_exist=comprobacion de path (path)
    if not path_exists:
        logger.debug("Error, Incorrect ImagePath value")
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Incorrect ImagePath value" )
        return jsonify(response)

    file_exists = imageAPI_utils.check_if_file_exists(args_dict['ImagePath'], args_dict['ImagefileName'])
    link_exists = imageAPI_utils.check_if_file_exists(args_dict['LinkPath'], args_dict['LinkName'])

    if not file_exists:
        logger.debug("Error, %s does not exist", file_exists)
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Invalid Image: image does not exist" )
        return jsonify(response)
    if link_exists:
        logger.debug("Error, %s already exist", file_exists)
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Invalid Link: link already exists" )
        return jsonify(response)

    path_exists = imageAPI_utils.check_if_path_exists(args_dict['LinkPath'])
    #path_exist=comprobacion de path (path)
    if not path_exists:
        imageAPI_utils.create_folder(args_dict['LinkPath'])

    create_link_response = imageAPI_utils.create_symbolic_link(args_dict['ImagePath'], args_dict['ImagefileName'], args_dict['LinkPath'], args_dict['LinkName'])
    if create_link_response[0] == False:
        response = build_response("SetSymbolicImage", "FAILURE", create_link_response[1], create_link_response[2] )
        return jsonify(response)

    response = build_response(function_type, "SUCCESS")    
    return jsonify(response)    


@app.route(prop.SERVER_URL + "UploadImage", methods=['POST'])
def upload_image():
    logger.info('Processing upload images request')
    args_dict = request.get_json()
    if not args_dict:
        response = build_response("UpdateImage", "FAILURE", "MISSING_PARAMETER", "Missing parameters json" )
        return jsonify(response)

    logger.debug('Received arguments: %s', str(args_dict))

    check_result = imageAPI_utils.check_image_parameters(args_dict)
    
    if check_result[0] == False:
        response = build_response("UploadImage", "FAILURE", check_result[1], check_result[2] )
        return jsonify(response)

    return jsonify(handle_image("UploadImage", args_dict ))    


@app.route(prop.SERVER_URL + "UpdateImage", methods=['POST'])
def update_image():
    logger.info('Processing update images request')
    args_dict = request.get_json()
    if not args_dict:
        response = build_response("UpdateImage", "FAILURE", "MISSING_PARAMETER", "Missing parameters json" )
        return jsonify(response)
    logger.debug('Received arguments: %s', str(args_dict))
    check_result = imageAPI_utils.check_image_parameters(args_dict)
    
    if check_result[0] == False:
        response = build_response("UpdateImage", "FAILURE", check_result[1], check_result[2] )
        return jsonify(response)

    return jsonify(handle_image("UpdateImage", args_dict ))   

@app.route(prop.SERVER_URL + "DeleteImage", methods=['GET'])
def delete_image():
    logger.info('Processing delete image request')
    args_dict = request.args.to_dict()
    logger.debug('Received arguments: %s', str(args_dict))
    check_result, error_type, error_message = imageAPI_utils.check_delete_image_parameters(args_dict)
    
    if check_result:
        relative_path = args_dict['path']
        file = args_dict['filename']
        delete_result, error_type, error_message = imageAPI_utils.delete_file(relative_path,file)
        return jsonify(build_response("DeleteImage", delete_result, error_type, error_message))
 
    else:
        return jsonify(build_response("DeleteImage","FAILURE", error_type, error_message ))

@app.route(prop.SERVER_URL + "DeleteFolder", methods=['GET'])
def delete_folder():
    logger.info('Processing delete folder request')
 
    args_dict = request.args.to_dict()
    logger.debug('Received arguments: %s', str(args_dict))

    check_result, error_type, error_message = imageAPI_utils.check_delete_folder_parameters(args_dict)
    if check_result:
        #return build_response("DeleteFolder","SUCCESS")
        relative_folder_path = args_dict['path']
        logger.debug("Path parameter value: %s",relative_folder_path)
        try:
            recursion_value = args_dict['recursive']
            
            if type(recursion_value) == 'unicode':
                recursion_value = recursion_value.encode('utf-8')
            
            force_recursion = True if recursion_value == 'TRUE' else False
            logger.debug("Force recursion parameter value: %s",force_recursion)
        except KeyError:
            force_recursion = False
            logger.debug("Recursion parameter not found. Force recursion parameter value: %s",force_recursion)

        

        delete_result, error_type, error_message = imageAPI_utils.delete_folder(relative_folder_path, force_recursion)
        return jsonify(build_response("DeleteFolder", delete_result, error_type, error_message))
    else:
        return jsonify(build_response("DeleteFolder","FAILURE",error_type, error_message))



def handle_image(function_type, args_dict):

    path_exists = imageAPI_utils.check_if_path_exists(args_dict['path'])
    #path_exist=comprobacion de path (path)
    if not path_exists:
        logger.debug("Error, Incorrect ImagePath value")
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Incorrect path value" )
        return response

    #file_exist = comprobacion de file  (file_name)
    
    #comprobacion de imagen (image_data) #mediante excepcion en data_to_image
    file_exists = imageAPI_utils.check_if_file_exists(args_dict['path'], args_dict['filename'])

    if function_type is "UpdateImage":
        if not file_exists:
            logger.debug("Error, %s does not exist", args_dict['filename'])
            response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Invalid Image: image does not exist" )
            return response

    elif function_type is "UploadImage":
        if file_exists:
            logger.debug("Error, %s already exists", args_dict['filename'])
            response = build_response(function_type, "FAILURE", "DUPLICATED_IMAGE", "Duplicated Image: image alredy exists" )
            return response
    else:
        raise 'invalid function_type. UploadImage or UpdateImage is required'         

    try:
        
        image = imageAPI_utils.base64_to_image(args_dict['imageData'])

    except TypeError:
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Error in imageData, invalid base64 format" )
        return response  
        
    except IOError:
        response = build_response(function_type, "FAILURE", "INVALID_PARAMETER", "Error in imageData, invalid image" )
        return response 

    imageAPI_utils.save_image(args_dict['path'], args_dict['filename'], image)    
 
    response = build_response(function_type, "SUCCESS")
    return response



#Returns dictionary with metadata information
def build_response(function_name, status, code = None, message = None):

    response = {
        'metadata':{"request": function_name,"timestamp": time.time()}
        
    }
    
    if status == 'SUCCESS':
        response['response'] = {"status": status}

    elif status == 'FAILURE':
        if not code or not message:
            raise 'incorrect response structure'

        response['response'] = {"code": code,"message": message,"status": status}

    return response






    